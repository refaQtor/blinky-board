
int domode = 0;

void setup() {
  pinMode(2, OUTPUT);
  digitalWrite(2, LOW);
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  digitalWrite(9, LOW);
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(14, INPUT_PULLUP);
  pinMode(15, INPUT_PULLUP);

  if (!(digitalRead(5))) domode = 1;
  if (!(digitalRead(6))) domode = 2;
  if (!(digitalRead(7))) domode = 3;
  if (!(digitalRead(5)) & !(digitalRead(6))) domode = 4;
  if (!(digitalRead(5)) & !(digitalRead(7))) domode = 5;
}

void beep( int times )
{
  if (times < 1) times = 1;
  for (int i = 0; i < times; i++)
  {
    digitalWrite(10, HIGH);
    delay(20);
    digitalWrite(10, LOW); 
    delay(20);
  }
}

void doModeZero()
{
  //traffic light sequence
 digitalWrite(2, HIGH); 
 delay(1500);
 digitalWrite(2, LOW);
 digitalWrite(3, HIGH);
 delay(500);
 digitalWrite(3, LOW);
 digitalWrite(4, HIGH);
 delay(1500); 
 digitalWrite(4, LOW);
}

void doModeOne()
{
  if (!(digitalRead(5))) 
  {
   digitalWrite(2, LOW);
   digitalWrite(10, HIGH);
  }
   else
   {
   digitalWrite(2, HIGH);
   digitalWrite(10, LOW);
   }
   
  if (!(digitalRead(6)))
  {
   digitalWrite(3, LOW);
   digitalWrite(10, HIGH);
  }
   else
   {
   digitalWrite(3, HIGH);
   digitalWrite(10, LOW);
   }
 
   if (!(digitalRead(7)))
   {
   digitalWrite(4, LOW);
   digitalWrite(10, HIGH);
  }
   else
   {
   digitalWrite(4, HIGH);
   digitalWrite(10, LOW);
   }
 }

void doModeTwo()
{
  int jigger = analogRead(A7);
  analogWrite(10, jigger);
}

void doModeThree()
{
  int jigger = analogRead(A6);
  if ((jigger > 512) & (jigger < 768))
  {
   digitalWrite(2, LOW);
   digitalWrite(3, HIGH);
   digitalWrite(4, LOW); 
  }
  if (jigger > 768) 
  {
   digitalWrite(2, LOW);
   digitalWrite(3, LOW);
   digitalWrite(4, HIGH); 
  }
  if (jigger < 512) 
  {
   digitalWrite(2, HIGH);
   digitalWrite(3, LOW);
   digitalWrite(4, LOW); 
  }
}

void doModeOneAndTwo()
{
   int pot = (analogRead(A6) +1);
   int on = ((1023-pot)/48)+2;
   int off = (pot/48)-2;
   digitalWrite(9, HIGH);
   delay(on);
   digitalWrite(9, LOW);
   delay(off);
}

void doModeOneAndThree()
{
  if ( !digitalRead(6) )
  {
   int pot = (analogRead(A6) +1);
   int on = ((1023-pot)/48)+2;
   int off = (pot/48)-2;
   digitalWrite(8, HIGH);
   delay(on);
   digitalWrite(8, LOW);
   delay(off);
  }
}

void loop() {
  switch (domode) {
  case 1:
  doModeOne();
  break;
  case 2:
  doModeTwo();
  break;
  case 3:
  doModeThree();
  break;
  case 4:
  doModeOneAndTwo();
  break;
  case 5:
  doModeOneAndThree();
  break;
  default:
  doModeZero();
  break;
  }
}
